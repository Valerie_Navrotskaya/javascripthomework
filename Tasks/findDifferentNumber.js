function findUnique(numeric) {
    const distinctNumeric = [...new Set(numeric)];
    return numeric.filter(data => data === distinctNumeric[0]).length === 1 ? distinctNumeric[0] : distinctNumeric[1];
}

const numeric = [5, 5, 5, 7, 5];
console.log(findUnique(numeric));