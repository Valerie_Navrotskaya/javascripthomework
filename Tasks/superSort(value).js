// use sort or reverse
//convert string into array//
let str = 'mic09ha1el 4b5en6 michelle be4atr3ice';
let arrStr = str.split(' ');
console.log(arrStr)

arrStr.sort((function(index){
    return function(a, b){
        var aIsLetter = a.match(/[a-z]/i),
            bIsLetter = b.match(/[a-z]/i);
        if (aIsLetter && !bIsLetter) return -1;
        if (!aIsLetter && bIsLetter) return 1;
        return (a[index] == b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
    };
})(2));
console.log(arrStr)
